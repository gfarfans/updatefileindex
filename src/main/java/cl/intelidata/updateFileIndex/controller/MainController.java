package cl.intelidata.updateFileIndex.controller;

import cl.intelidata.updateFileIndex.service.IProcesoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
@Slf4j
public class MainController {

    @Autowired
    private IProcesoService procesoService;

    public void process() {
        log.info("-- Procesar Controller --");
        procesoService.procesar();
    }




}
