package cl.intelidata.updateFileIndex.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Getter
@Setter
@PropertySource("file:${properties.externo}")
public class PropertiesExt {

    @Value("${app.delimitador.fila}")
    private String saltoLinea;

    @Value("${app.charset}")
    private String charsetApp;

}
