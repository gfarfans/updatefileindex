package cl.intelidata.updateFileIndex;

import cl.intelidata.updateFileIndex.controller.MainController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static cl.intelidata.updateFileIndex.Util.Constants.ERROR_CODE;
import static cl.intelidata.updateFileIndex.Util.Constants.OK_CODE;

@Slf4j
@SpringBootApplication
public class UpdateFileIndexApplication implements CommandLineRunner {

	@Autowired
	private MainController controller;

	@Autowired
	ApplicationArguments arguments;

	@Override
	public void run(String... args) throws Exception {

		log.info("- Inicio Actualización de Archivo Index -");

		log.info("Args Esperados(4) : [Archivo Index, Archivo Reporte, Archivo Temporal, Archivo Salida]");

		log.info("Args Recibidos({}) : {} ", arguments.getNonOptionArgs().size(), arguments.getNonOptionArgs());

		log.info("Tamaño de Argumentos Recibidos = {}", arguments.getNonOptionArgs().size());

		if (arguments.getNonOptionArgs().size() == 4) {
			log.info("Inicio Proceso");
			controller.process();
		} else {
			log.info("Error en Cantidad de Argumentos recibidos, ({})", arguments.getNonOptionArgs().size());
			System.exit(ERROR_CODE);
		}

		log.info("Proceso Terminado con éxito");
		System.exit(OK_CODE);
	}

	public static void main(String[] args) {

		SpringApplication.run(UpdateFileIndexApplication.class, args);
	}


}
