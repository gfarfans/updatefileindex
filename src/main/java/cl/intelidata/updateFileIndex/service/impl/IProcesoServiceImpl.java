package cl.intelidata.updateFileIndex.service.impl;

import cl.intelidata.updateFileIndex.Util.Util;
import cl.intelidata.updateFileIndex.config.PropertiesExt;
import cl.intelidata.updateFileIndex.model.api.AcumulaRecibo;
import cl.intelidata.updateFileIndex.model.api.RegistroIndex;
import cl.intelidata.updateFileIndex.model.api.RegistroReporte;
import cl.intelidata.updateFileIndex.service.IProcesoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static cl.intelidata.updateFileIndex.Util.Constants.*;

@Service
@Slf4j
public class IProcesoServiceImpl implements IProcesoService {

    @Autowired
    ApplicationArguments arguments;

    @Autowired
    private PropertiesExt propertiesExt;

    private HashMap<String, RegistroReporte> listadoReporte;

    private List<AcumulaRecibo> listadoRecibos;

    private AcumulaRecibo acumulado;

    private List<String> contentIndex;

    private List<String> contentFinal;

    private int indexAcumulado;

    @Override
    public void procesar() {

        log.info("--- Procesar Service ---");

        log.info("Actualizacion archivo index clientes UNI");

        listadoReporte = new HashMap<>();
        listadoRecibos = new ArrayList<>();

        cargaReporte();
        procesaIndex();
        generaNuevoIndex();
    }

    private void generaNuevoIndex() {
        log.info("Generacion nuevo archivo index");
        log.info("Archivo de salida = {} ", arguments.getNonOptionArgs().get(3));

        indexAcumulado = 0;
        contentFinal = new ArrayList<>();

        log.info("Tamaño Listado Reporte = {}", listadoReporte.size());
        log.info("Tamaño Listado Recibos = {}", listadoRecibos.size());

        if (listadoRecibos.size() == 0) {
            log.info("[WARNING] Cantidad de Recibos es igual a Cero(0), posible Error");
        }

        contentIndex.forEach(line -> {

            RegistroIndex index = new RegistroIndex(getSubString(line));

            AcumulaRecibo acumulado = listadoRecibos.get(indexAcumulado);

            RegistroReporte report = listadoReporte.get(index.getSortIndex());

            if (acumulado.getSortIndexs().contains(index.getSortIndex())) {

                contentFinal.add(index.getNewRegistroIndex(line, report, acumulado, indexAcumulado + 1));
            } else {

                indexAcumulado++;

                try {
                    acumulado = listadoRecibos.get(indexAcumulado);
                } catch (Exception ex) {

                    log.error("Actual: " + index.getSortIndex(), ex);

                    for (String sort : acumulado.getSortIndexs()) {

                        log.error(sort);
                    }
                }
                contentFinal.add(index.getNewRegistroIndex(line, report, acumulado, indexAcumulado + 1));
            }
        });

        Util.createFile(EMPTY, arguments.getNonOptionArgs().get(3), contentFinal,
                propertiesExt.getSaltoLinea(), propertiesExt.getCharsetApp());

    }

    private void procesaIndex() {

        log.info("Logicas archivo index");
        log.info("Archivo index = {} ", arguments.getNonOptionArgs().get(0));

        contentIndex = Util.getFileContent(EMPTY, arguments.getNonOptionArgs().get(0), propertiesExt.getCharsetApp());

        log.info("Cantidad de lineas encontradas en archivo (No vacias) = {}", contentIndex.size());

        acumulado = null;

        contentIndex.forEach(line -> {

            RegistroIndex index = new RegistroIndex(getSubString(line));

            RegistroReporte report = listadoReporte.get(index.getSortIndex());

            if (report != null) {

                if (acumulado == null) {

                    acumulado = new AcumulaRecibo(report);

                } else {

                    boolean agregoRecibo = acumulado.addRecibo(report);

                    if (!agregoRecibo) {
                        listadoRecibos.add(acumulado);
                        acumulado = new AcumulaRecibo(report);
                    }
                }
            } else {
                log.info(":::: Report es null!!, index = {}", index);
            }
        });

        if (acumulado != null) {
            log.info("Revisión Acumulado");
            if (!listadoRecibos.contains(acumulado)) {
                listadoRecibos.add(acumulado);
            }
        }
    }

    private String getSubString(String line) {

        try {

            return line.substring(0, 20).trim();

        } catch (Exception e) {
            log.error("Error en linea a procesar = {}, (minimo 20 Tamaño)", line);
            System.exit(ERROR_CODE);
        }

        return EMPTY;
    }

    private void cargaReporte() {

        log.info("Carga archivo reporte");
        log.info("Archivo de reporte = {} ", arguments.getNonOptionArgs().get(1));

        List<String> content = Util.getFileContent(EMPTY, arguments.getNonOptionArgs().get(1), propertiesExt.getCharsetApp());

        log.info("Cantidad de lineas encontradas en archivo (No vacias) = {}", content.size());

        content.forEach(line -> {

            List<String> lineList = Util.getDataSeparator(line, ";");

            if (lineList.size() == 6) {
                RegistroReporte tmp = new RegistroReporte(lineList);
                listadoReporte.put(tmp.getSortIndex(), tmp);
            } else {
                log.error("Error en linea a procesar = {}", line);
                System.exit(ERROR_CODE);
            }
        });

    }
}
