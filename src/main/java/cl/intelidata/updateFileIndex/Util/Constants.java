package cl.intelidata.updateFileIndex.Util;

public class Constants {

    //GENERICS
    public static final String EMPTY = "";
    public static final String CHARSET_ISO = "ISO-8859-1";
    public static final Integer ERROR_CODE = 1;
    public static final Integer OK_CODE = 0;

}
