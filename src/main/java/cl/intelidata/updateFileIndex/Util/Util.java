package cl.intelidata.updateFileIndex.Util;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static cl.intelidata.updateFileIndex.Util.Constants.EMPTY;
import static cl.intelidata.updateFileIndex.Util.Constants.ERROR_CODE;

@Slf4j
public class Util {

    /**
     * Metodo que lee el contenido de un archivo
     *
     * @param path
     * @param fileName
     * @return
     */
    public static List<String> getFileContent(String path, String fileName, String charsetName) {

        String ruta = path + fileName;

        log.info("Obtener contenido del archivo = {}", ruta);

        List<String> stringList = new ArrayList<>();

        try {
            BufferedReader b = new BufferedReader(new InputStreamReader(new FileInputStream(ruta), charsetName));
            stringList = b.lines().filter(x -> !x.trim().equals(EMPTY)).collect(Collectors.toList());
            b.close();
        } catch (Exception e) {
            log.error("No se pudo leer el contenido del archivo = {}", ruta);
            log.error(e.getMessage(), e);
            System.exit(ERROR_CODE);
        }

        return stringList;
    }

    /**
     *
     * @param content
     * @param delimitador
     * @return
     */
    public static List<String> getDataSeparator(String content, String delimitador) {

        try {

            String[] array = content.split("[" + delimitador + "]", -1);
            return Arrays.asList(array);

        } catch (Exception e) {
            log.error("Error en Contenido a procesar = {}, delimitador = {}", content, delimitador);
            log.error("Estructura de Linea incorrecta, Metodo getData()");
            System.exit(ERROR_CODE);
        }

        return null;
    }

    /**
     * Crear Archivo
     * @param path
     * @param fileName
     * @param contentList
     * @param delimitador
     * @param charsetName
     * @return
     */
    public static boolean createFile(String path, String fileName, List<String> contentList,
                                     String delimitador, String charsetName) {

        String finalName = path + fileName;

        log.info("Inicio de creacion de archivo = {} ", finalName);

        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(finalName, true),
                charsetName))) {

            StringBuilder data = new StringBuilder();
            contentList.forEach(item -> data.append(item).append(delimitador));
            writer.write(data.toString());

            log.info("Se creó el archivo con exito " + fileName);
            return true;

        } catch (Exception e) {
            log.error("Ocurrio un error en la creacion de Archivo" + fileName);
            log.error(e.toString());
            System.exit(ERROR_CODE);

        }

        return false;
    }
}
