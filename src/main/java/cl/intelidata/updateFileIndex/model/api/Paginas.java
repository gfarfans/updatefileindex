package cl.intelidata.updateFileIndex.model.api;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Paginas {
    private int paginaInicial;
    private int paginaFinal;

    public Paginas(int totalPagAcumu, int totalPagRecibo) {
        if (totalPagAcumu == 0) {
            this.paginaInicial = 1;
        } else {
            this.paginaInicial = (totalPagAcumu + 1);
        }
        this.paginaFinal = (this.paginaInicial - 1 + totalPagRecibo);
    }


}