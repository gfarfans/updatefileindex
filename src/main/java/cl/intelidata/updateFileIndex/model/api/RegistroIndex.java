package cl.intelidata.updateFileIndex.model.api;
import lombok.*;

@ToString
@NoArgsConstructor
@Getter
@Setter
public class RegistroIndex {

    private String sortIndex;

    public RegistroIndex(String line) {

        setSortIndex(line);
    }

    public String getNewRegistroIndex(String indexOri, RegistroReporte report, AcumulaRecibo acumulado, int index) {
        Paginas pag = acumulado.getPaginasBySortIndex(report.getSortIndex());
        String paginaIncial = String.format("%010d", new Object[]{Integer.valueOf(pag.getPaginaInicial())});
        String paginaFinal = String.format("%010d", new Object[]{Integer.valueOf(pag.getPaginaFinal())});
        String totalPaginas = String.format("%010d", new Object[]{Integer.valueOf(acumulado.getNumPaginas())});
        String numeroCliente = String.format("%010d", new Object[]{Integer.valueOf(index)});

        String cadena = indexOri;

        while (cadena.contains("\\n")) {
            String tmp1 = cadena.substring(0, cadena.indexOf("\\n"));
            String tmp2 = cadena.substring(cadena.indexOf("\\n") + 1);
            cadena = tmp1 + " " + tmp2;
        }
        return cadena.replaceAll("\r\n", "") + " " + paginaIncial + " " + paginaFinal + " " + totalPaginas + " " + numeroCliente + " ";
    }


}