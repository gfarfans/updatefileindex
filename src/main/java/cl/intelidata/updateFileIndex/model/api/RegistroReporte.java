package cl.intelidata.updateFileIndex.model.api;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RegistroReporte {
    private String cuenta;
    private String direccion;
    private String tipoDoc;
    private String numDoc;
    private String numPaginas;
    private String sortIndex;

    public RegistroReporte(List<String> line) {

        setCuenta(line.get(0).trim());
        setDireccion(line.get(1).trim());
        setTipoDoc(line.get(2).trim());
        setNumDoc(line.get(3).trim());
        setNumPaginas(line.get(4).trim());
        setSortIndex(line.get(5).trim());
    }

    public String getKey() {
        return this.cuenta + "-" + this.direccion + "-" + this.tipoDoc + "-" + this.numDoc;
    }


}