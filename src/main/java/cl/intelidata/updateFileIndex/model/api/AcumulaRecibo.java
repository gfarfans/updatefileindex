package cl.intelidata.updateFileIndex.model.api;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AcumulaRecibo {
    private int numPaginas;
    private List<String> sortIndexs;
    private List<Paginas> paginas;
    private String key;

    public AcumulaRecibo(RegistroReporte report) {

        this.key = report.getKey();
        this.numPaginas = 0;
        this.sortIndexs = new ArrayList<>();
        this.paginas = new ArrayList<>();
        addRecibo(report);
    }

    public boolean addRecibo(RegistroReporte report) {

        if (this.key.equals(report.getKey())) {
            int numPages = Integer.parseInt(report.getNumPaginas());

            Paginas pagina = new Paginas(this.numPaginas, numPages);

            this.numPaginas += numPages;
            this.sortIndexs.add(report.getSortIndex());
            this.paginas.add(pagina);
            return true;
        }
        return false;
    }

    public Paginas getPaginasBySortIndex(String sortIndex) {
        return this.paginas.get(this.sortIndexs.indexOf(sortIndex));
    }


}